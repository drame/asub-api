package com.asub.dao;


import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.asub.entities.Article;
import com.asub.entities.Evenement;
import com.asub.entities.Publicite;


public class SpecificationDao {

	/* public static Specification<Question> avecTitre(String titre){
		 return new Specification<Question>(){

			@Override
			public Predicate toPredicate(Root<Question> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				if(!titre.isEmpty()){
					return cb.like(root.get("titre"),titre);
				}
				return null;
			}
			 
		 };
	 }*/
	public static Specification<Article> ArticleAvecCategorie(String categorie){
		return new Specification<Article>(){

			@Override
			public Predicate toPredicate(Root<Article> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				if(categorie.isEmpty())
				return null;
				else{
					return cb.equal(root.get("categorie"), categorie);
				}
			}
			
		};
	}
	public static Specification<Article> ArticleAvecType(String[] type){
		return new Specification<Article>(){

			@Override
			public Predicate toPredicate(Root<Article> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				if(type.length==0)
				return null;
				else{
					return root.get("type").in(type);
				}
			}
			
		};
	}
	public static Specification<Article> ArticleAvecPays(String pays){
		return new Specification<Article>(){

			@Override
			public Predicate toPredicate(Root<Article> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				if(pays.isEmpty())
				return null;
				else{
					return cb.equal(root.get("pays"), pays);
				}
			}
			
		};
	}
	public static Specification<Article> ArticleAvecTitre(String titre){
		return new Specification<Article>(){

			@Override
			public Predicate toPredicate(Root<Article> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				if(titre.isEmpty())
				return null;
				else{
					return cb.like(root.get("titre"), "%"+titre+"%");
				}
			}
			
		};
	}
	public static Specification<Article> ArticleAvecAuteur(Long id){
		return new Specification<Article>(){

			@Override
			public Predicate toPredicate(Root<Article> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				if(id==null||id==0)
				return null;
				else{
					return cb.equal(root.get("auteur").get("id"), id);
				}
			}
			
		};
	}
	public static Specification<Article> ArticleDifferent(Long id){
		return new Specification<Article>(){

			@Override
			public Predicate toPredicate(Root<Article> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
					return cb.notEqual(root.get("id"), id);
			}
			
		};
	}
	public static Specification<Article> ArticleAvecVisibilite(boolean visibilite){
		return new Specification<Article>(){

			@Override
			public Predicate toPredicate(Root<Article> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
					return cb.equal(root.get("visibilite"), visibilite);
				
			}
			
		};
	}
	public static Specification<Evenement> EvenementAvecTitre(String titre){
		return new Specification<Evenement>(){

			@Override
			public Predicate toPredicate(Root<Evenement> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				if(titre.isEmpty())
				return null;
				else{
					return cb.like(root.get("titre"), "%"+titre+"%");
				}
			}
			
		};
	}
	public static Specification<Evenement> EvenementDifferent(Long id){
		return new Specification<Evenement>(){

			@Override
			public Predicate toPredicate(Root<Evenement> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
					return cb.notEqual(root.get("id"), id);
				
			}
			
		};
	}
	public static Specification<Evenement> EvenementAvecCategorie(String categorie){
		return new Specification<Evenement>(){

			@Override
			public Predicate toPredicate(Root<Evenement> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				if(categorie.isEmpty())
				return null;
				else{
					return cb.equal(root.get("categorie"), categorie);
				}
			}
			
		};
	}
	public static Specification<Evenement> EvenementAvecType(String type){
		return new Specification<Evenement>(){

			@Override
			public Predicate toPredicate(Root<Evenement> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				if(type.isEmpty())
				return null;
				else{
					return cb.equal(root.get("type"), type);
				}
			}
			
		};
	}
	public static Specification<Evenement> EvenementAvecDateFin(Date actuelle){
		return new Specification<Evenement>(){

			@Override
			public Predicate toPredicate(Root<Evenement> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				if(actuelle==null)return null;
				return cb.greaterThanOrEqualTo(root.get("fin"), actuelle);
			}
			
		};
	}
	public static Specification<Evenement> EvenementAvecVisibilite(boolean visibilite){
		return new Specification<Evenement>(){

			@Override
			public Predicate toPredicate(Root<Evenement> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub

					return cb.equal(root.get("visibilite"), visibilite);
			}
			
		};
	}
	public static Specification<Publicite> PubliciteAvecVisibilite(Boolean visibilite){
		return new Specification<Publicite>(){

			@Override
			public Predicate toPredicate(Root<Publicite> root, CriteriaQuery<?> arg1, CriteriaBuilder cb) {
				// TODO Auto-generated method stub
				    if(visibilite==null){
				    	return null;
				    }
					return cb.equal(root.get("visibilite"), visibilite);
			}
			
		};
	}	
	
	
}
