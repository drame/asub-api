package com.asub.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.asub.entities.Article;

public interface ArticleRepository extends JpaRepository<Article, Long>,JpaSpecificationExecutor<Article> {
	@Query("select a from Article a where a.type=?1 order by creation asc")
public Page<Article> getArticles(String type,Pageable p);
	public Page<Article> findAllByOrderByCreationAsc(Pageable p);
	@Query("select a from Article a where a.type=?1 and a.visibilite=true order by creation asc")
public Page<Article> getVisiblesArticles(String type,Pageable p);
	@Query("select a from Article a where  a.visibilite=true order by creation asc")
	public Page<Article> findVisibiliteByOrderByCreationAsc(Pageable p);
	@Query("select a from Article a where  a.visibilite=true and a.id<>?1 and a.categorie=?2  order by creation asc ")
	public Page<Article> articleAssocie(Long id,String categorie,Pageable p);
	public List<Article> findBySymboleAndVisibilite(String s,boolean v);
}
