package com.asub.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.asub.entities.Publicite;

public interface PubliciteRepository extends JpaRepository<Publicite, Long>,JpaSpecificationExecutor<Publicite> {
}
