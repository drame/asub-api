package com.asub.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.asub.entities.Donnees;
import com.asub.entities.Structure;

public interface DonneesRepository extends JpaRepository<Donnees, Long>{
@Query("select d from Donnees d inner join d.structure s where s.symbole=:symbole ORDER BY date ASC  ")
public Page<Donnees> findByStructureOrderByDate(@Param("symbole") String s,Pageable p);
@Query("select d from Donnees d inner join d.structure s where s.symbole=:symbole ORDER BY date ASC  ")
public List<Donnees> findAllByStructureOrderByDate(@Param("symbole") String s);
public List<Donnees> findByStructure(Structure s);
@Modifying
@Query("delete from Donnees d where d.structure.symbole=?1")
public void deleteStructureDonnees(String s);
}
