package com.asub.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.asub.entities.Article;
import com.asub.entities.Evenement;

public interface EvenementRepository extends JpaRepository<Evenement,Long>,JpaSpecificationExecutor<Evenement> {
public Page<Evenement> findAllByOrderByDateAsc(Pageable p);
@Query("select e from Evenement e where e.type=?1  order by date asc")
public Page<Evenement> findByType(String type,Pageable p);
@Query("select e from Evenement e where e.type=?1 and e.visibilite=true order by date asc")
public Page<Evenement> findVisiblesEvenementsByType(String type, Pageable p);
@Query("select e from Evenement e where e.categorie=?1 and  e.type=?2 and e.visibilite=true order by date asc")
public  Page<Evenement> findVisibiliteAndCategoryAndTypeByOrderByAsc(String c,String type,Pageable p);
}
