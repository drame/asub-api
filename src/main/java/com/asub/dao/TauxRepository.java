package com.asub.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.asub.entities.Taux;

public interface TauxRepository extends JpaRepository<Taux, String> {

}
