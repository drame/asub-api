package com.asub.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.asub.entities.Structure;

public interface StructureRepository extends JpaRepository<Structure, String> {
	public List<Structure> findBySymbole(String s);
	public List<Structure> findByNom(String s);

}
