package com.asub.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.asub.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {
public User findByEmail(String email);
}
