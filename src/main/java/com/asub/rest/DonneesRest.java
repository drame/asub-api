package com.asub.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asub.AsubException;
import com.asub.View;
import com.asub.entities.Donnees;
import com.asub.metier.IDonnees;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
@CrossOrigin(origins = {"*"}, maxAge = 4800, allowCredentials = "true")public class DonneesRest {
@Autowired
private IDonnees iDonnees;
@RequestMapping(value="/donnee",method=RequestMethod.POST)
public Object addDonnees(@RequestBody Donnees d) {
	Map<String,Object> m= new HashMap<>();
	try {
		iDonnees.addDonnees(d);
		m.put("error", false);
	} catch (AsubException e) {
		// TODO Auto-generated catch block
		m.put("donnee", e.getMessage());
	}
	return m;
}
@RequestMapping(value="/donnee",method=RequestMethod.DELETE)
public Object deleteDonnees(@RequestParam(defaultValue="0")Long id) {
	Map<String,Object> m= new HashMap<>();
	iDonnees.deleteDonnees(id);
	m.put("error", false);
	return m;
}
@JsonView({View.Donnees.class})
@RequestMapping(value="/donnee",method=RequestMethod.GET)
public Donnees getDonnees(@RequestParam(defaultValue="0")Long id) {
	return iDonnees.getDonnees(id);
}
@JsonView({View.Donnees.class})
@RequestMapping(value="/donnees",method=RequestMethod.GET)
public Page<Donnees> getAllDonnees(@RequestParam(defaultValue="0")String s, @RequestParam(defaultValue="0")int page, @RequestParam(defaultValue="0")int size) {
	return iDonnees.getAllDonnees(s, page, size);
}
@JsonView({View.Donnees.class})
@RequestMapping(value="/donnees/get",method=RequestMethod.GET)
public List<Donnees> allDonnees(@RequestParam(defaultValue="0")String s) {
	return iDonnees.getAllDonnees2(s);
}
@JsonView({View.Donnees1.class})
@RequestMapping(value="/palmares",method=RequestMethod.GET)
public Object palmares() {
	return iDonnees.palmares();
}
}
