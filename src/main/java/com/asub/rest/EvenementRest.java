package com.asub.rest;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.asub.entities.Article;
import com.asub.entities.Evenement;
import com.asub.metier.IArticle;
import com.asub.metier.IEvenement;
@RestController
@CrossOrigin(origins = {"*"}, maxAge = 4800, allowCredentials = "true")public class EvenementRest {
@Autowired
private IEvenement iEv;
@RequestMapping(value="/evenement",method=RequestMethod.POST, consumes = "multipart/form-data")
public Object addArticle(@RequestPart(required=true) @Valid Evenement evenement,BindingResult br,@RequestPart(required=false) MultipartFile photo) {
	Map<String,Object> m= new HashMap<>();
	if(br.hasErrors()){
		m.put("error", true);
		for(FieldError fe : br.getFieldErrors()){
			m.put(fe.getField(), fe.getDefaultMessage());
		}
		
		return m;
	}
	try {
		iEv.addEvenement(evenement,photo);
		m.put("error", false);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		System.out.println(e);
		m.put("error", true);
		m.put("evenement", e.getMessage());
	}
	
	return m;
}
@RequestMapping(value="/evenement")
public Object getEvenement(@RequestParam(defaultValue="0") Long id) {
	return iEv.getEvenement(id);
}
// il ne faut securiser cette methide mais celle d'au dessus
@RequestMapping(value="/evenement/get")
public Object getEvenement1(@RequestParam(defaultValue="0") Long id) {
	Evenement e= iEv.getEvenement(id);
	Map<String, Object> m= new HashMap<>();
	m.put("error", true);
	if(e!=null){
		if(e.isVisibilite()){
			return e;
		}
	}
	return m;
}
@RequestMapping(value="/evenement",method=RequestMethod.DELETE)
public Object deleteEvenement(@RequestParam(defaultValue="0") Long id) {
	iEv.deleteEvenement(id);
	Map<String,Object> m= new HashMap<>();
	return m;
}
@RequestMapping(value="/evenement",method=RequestMethod.PUT)
public Object changeStatus(@RequestParam(defaultValue="0")Long id,@RequestParam(defaultValue="false") boolean v) {
	iEv.changeStatus(id,v);
	Map<String,Object> m= new HashMap<>();
	return m;
}
@RequestMapping(value="/evenements")
public Page<Evenement> getEvenements(@RequestParam(defaultValue="") String type,@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="10")int size) {
	return iEv.getEvenementByType(type,new PageRequest(page, size));
}
// a ne pas securiser
@RequestMapping(value="/evenements/criteria")
public Page<Evenement> getEvenementsWithCriteria(@RequestParam(defaultValue="") String type,
		@RequestParam(defaultValue="") String categorie,
		@RequestParam(defaultValue="") String titre,
		@RequestParam(defaultValue="") String lieu,
		@RequestParam(defaultValue="null") String fin,
		@RequestParam(defaultValue="0") int page, 
		@RequestParam(defaultValue="10")int size) {
	SimpleDateFormat sd= new SimpleDateFormat("dd-MM-yyyy");
	Date date=null;
	try {
		date = sd.parse(fin);
		System.out.println(date);
	} catch (ParseException e) {
		// TODO Auto-generated catch block
	}
	return iEv.getEvenements(type,categorie,titre,lieu,date,true,new PageRequest(page, size));
}
@RequestMapping(value="/evenements/get")
public Page<Evenement> getVisiblesEvenements(@RequestParam(defaultValue="") String type,@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="10")int size) {
	return iEv.getVisiblesEvenementsByType(type,new PageRequest(page, size));
}
@RequestMapping(value="/evenements/associe")
public Page<Evenement> getEvenementsAssocies(@RequestParam(defaultValue="") String type,@RequestParam(defaultValue="") String categorie,@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="10")int size) {
	return iEv.evenementAssocie(type,categorie,new PageRequest(page, size));
}
}
