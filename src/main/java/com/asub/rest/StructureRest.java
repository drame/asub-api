package com.asub.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.asub.AsubException;
import com.asub.View;
import com.asub.entities.Structure;
import com.asub.metier.IStructure;
import com.fasterxml.jackson.annotation.JsonView;

@RestController
@CrossOrigin(origins = {"*"}, maxAge = 4800, allowCredentials = "true")public class StructureRest {
@Autowired
private IStructure iStructure;
@JsonView({View.Structure.class})
@RequestMapping(value="/structures")
public Page<Structure> getStructures(@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="10")int size) {
	return iStructure.getStructures(page, size);
}
@JsonView({View.Structure.class})
@RequestMapping(value="/allStructures")
public List<Structure> getAllStructures() {
	return iStructure.getAllStructures();
}
@JsonView({View.Structure.class})
@RequestMapping(value="/structure")
public Structure getStructure(@RequestParam(defaultValue="") String s) {
	return iStructure.getStructure(s);
}
@JsonView({View.Structure.class})
@RequestMapping(value="/structure",method=RequestMethod.PUT)
public Structure editStructure(@RequestBody Structure s) {
	return iStructure.editStructure(s);
}
@RequestMapping(value="/structure",method=RequestMethod.POST)
public Object addStructure(@RequestBody @Valid Structure s,BindingResult br) {
	Map<String,Object> m= new HashMap<>();
	if(br.hasErrors()){
		for(FieldError fe :br.getFieldErrors()){
			m.put("structure", fe.getDefaultMessage());
			return m;
		}
	}
	try {
		iStructure.addStructure(s);
		m.put("error", false);
		return m;
	} catch (AsubException e) {
		// TODO Auto-generated catch block
		System.out.println("except");
		m.put("structure", e.getMessage());
		return m;
	}
	
}
@RequestMapping(value="/structure",method=RequestMethod.DELETE)
public Object deleteStructure(@RequestParam(defaultValue="") String s) {
	iStructure.deleteStructure(s);
	Map<String,Object> m= new HashMap<>();
	m.put("error", false);
	return m;
}
@RequestMapping(value="/structures",method=RequestMethod.DELETE)
public Object deleteStructures() {
	iStructure.deleteAll();
	Map<String,Object> m= new HashMap<>();
	m.put("error", false);
	return m;
}
@JsonView({View.StructureWithBrmv.class})
@RequestMapping(value="/structures/get")
public List<Structure> getStructures(@RequestParam(defaultValue="0") int brvm){
	return iStructure.getStructure(brvm);
}
@RequestMapping(value = "/import", method = RequestMethod.POST, consumes = "multipart/form-data")
public Object ImportExcel(@RequestPart("file") MultipartFile excelfile) {	
	Map<String,Object> m= new HashMap<>();
	try {
		 iStructure.importExcel(excelfile);
			m.put("error", false);
	} catch (Exception e) {
		e.printStackTrace();
		m.put("error", true);
	}
 
	return m;
}
}
