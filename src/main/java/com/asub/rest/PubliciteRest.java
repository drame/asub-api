package com.asub.rest;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.asub.entities.Article;
import com.asub.entities.Publicite;
import com.asub.metier.IPublicite;

@RestController
@CrossOrigin(origins = {"*"}, maxAge = 4800, allowCredentials = "true")public class PubliciteRest {
	@Autowired
	private IPublicite rep;
	@RequestMapping(value="/publicite",method=RequestMethod.POST, consumes = "multipart/form-data")
	public Object addPub(@RequestPart(required=true) @Valid Publicite publicite,BindingResult br,@RequestPart(required=false) MultipartFile photo ){
		Map<String,Object> m= new HashMap<>();
		if(br.hasErrors()){
			m.put("error", true);
			for(FieldError fe : br.getFieldErrors()){
				m.put(fe.getField(), fe.getDefaultMessage());
			}
			
			return m;
		}
		try {
			rep.addPublicite(publicite, photo);
			m.put("error", false);
		}
		catch (Exception e) {
			// TODO: handle exception
			m.put("error", true);
			m.put("publicite", e.getMessage());
		}
		return m;
	}
	@RequestMapping(value="/publicite",method=RequestMethod.DELETE)
	public Object deleteArticle(@RequestParam(defaultValue="0") Long id) {
		rep.deletePublicite(id);
		Map<String,Object> m= new HashMap<>();
		return m;
	}
	@RequestMapping(value="/publicite",method=RequestMethod.PUT)
	public Object changeStatus(@RequestParam(defaultValue="0")Long id,@RequestParam(defaultValue="false") boolean v) {
		rep.changeStatus(id,v);
		Map<String,Object> m= new HashMap<>();
		return m;
	}
	@RequestMapping(value="/publicites")
	public Page<Publicite> getAllPubs(@RequestParam(defaultValue="") Boolean v,@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="10")int size) {
		return rep.getPublicites(v, size, page);
	}
	@RequestMapping(value="/publicite")
	public Publicite getPub(@RequestParam(defaultValue="") Long id) {
		return rep.getPublicite(id);
	}
	//ne pas securiser
	@RequestMapping(value="/publicites/get")
	public Object getPubs() {
		return rep.getPublicites(true, 3, 0).getContent();
	}
}
