package com.asub.rest;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asub.AsubException;
import com.asub.View;
import com.asub.entities.User;
import com.asub.metier.IUser;
import com.fasterxml.jackson.annotation.JsonView;
@RestController
@CrossOrigin(origins = {"*"}, maxAge = 4800, allowCredentials = "true")
public class UserRest {
@Autowired
private IUser iU;
@RequestMapping(value="/user", method=RequestMethod.POST)
public Object addUser(@RequestBody @Valid User u, BindingResult br){
	Map<String, Object> m= new HashMap<>();
	if(br.hasErrors()){
	m.put("error", true);
	for(FieldError fe : br.getFieldErrors()){
		m.put("user",fe.getField()+": "+fe.getDefaultMessage());
		return m;
	}
	
	}
	try {
		iU.addUser(u);
		m.put("error", false);
	} catch (AsubException e) {
		// TODO Auto-generated catch block
		m.put("error", true);
		m.put("user",e.getMessage());
	}
	return m;
}
@RequestMapping(value="/user")
public Object getCurrentUser(Principal p){

	User u= iU.getUser(p.getName());
	com.asub.pojo.Users user= new com.asub.pojo.Users(u.getEmail(),u.getNom(),u.getRole(),true);
	return user;
}
@RequestMapping(value="/users")
@JsonView({View.User.class})
public Page<User> getPageOfUser(@RequestParam(defaultValue="0") int page,
		@RequestParam(defaultValue="10")int size) {
	return iU.getPageOfUser(new PageRequest(page,size));
}
@RequestMapping(value="/users/get")
@JsonView({View.UserFront.class})
public List<User> getAllUser() {
	return iU.getAllUser();
}
@RequestMapping(value="/user",method=RequestMethod.DELETE)
public Object deleteUser(@RequestParam(defaultValue="0") Long id) {
	Map<String, Object> m= new HashMap<>();
	iU.deleteUser(id);
	m.put("error", false);
	return m;
	
}

}
