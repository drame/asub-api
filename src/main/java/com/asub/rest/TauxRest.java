package com.asub.rest;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.asub.entities.Taux;
import com.asub.metier.ITaux;

@RestController
@CrossOrigin(origins = {"*"}, maxAge = 4800, allowCredentials = "true")
public class TauxRest {
	@Autowired
private ITaux iTaux;
	@RequestMapping(value="/taux")
	public List<Taux> getAll(){
		return iTaux.getTaux();
	}
	@RequestMapping(value="/taux/update",method=RequestMethod.POST)
	public Object update(@RequestBody List<Taux> taux){
		 return iTaux.update(taux);
	}
	
}
