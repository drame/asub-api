package com.asub.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.asub.View;
import com.asub.entities.Article;
import com.asub.metier.IArticle;
import com.fasterxml.jackson.annotation.JsonView;
@RestController
@CrossOrigin(origins = {"*"}, maxAge = 4800, allowCredentials = "true")public class ArticleRest {
@Autowired
private IArticle iArticle;
@RequestMapping(value="/article",method=RequestMethod.POST, consumes = "multipart/form-data")
public Object addArticle(@RequestPart(required=true) @Valid Article article,BindingResult br,@RequestPart(required=false) MultipartFile photo) {
	if(article!=null){
		System.out.println(article.getTitre());
		article.setCreation(new Date());
	}
	Map<String,Object> m= new HashMap<>();
	if(br.hasErrors()){
		m.put("error", true);
		for(FieldError fe : br.getFieldErrors()){
			m.put(fe.getField(), fe.getDefaultMessage());
		}
		
		return m;
	}
	try {
		iArticle.addArticle(article,photo);
		m.put("error", false);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		System.out.println(e);
		m.put("error", true);
		m.put("article", e.getMessage());
	}
	
	return m;
}
@RequestMapping(value="/article")
@JsonView({View.Article.class})
public Object getArticle(@RequestParam(defaultValue="0") Long id) {
	return iArticle.getArticle(id);
}
@JsonView({View.Article.class})
@RequestMapping(value="/article/get")
public Object getArticle1(@RequestParam(defaultValue="0") Long id) {
	Article a= iArticle.getArticle(id);
	Map<String, Object> m= new HashMap<>();
	m.put("error", true);
	if(a!=null){
		if(a.isVisibilite()){
			return a;
		}
	}
	return m;
}
@RequestMapping(value="/article",method=RequestMethod.DELETE)
public Object deleteArticle(@RequestParam(defaultValue="0") Long id) {
	iArticle.deleteArticle(id);
	Map<String,Object> m= new HashMap<>();
	return m;
}
@RequestMapping(value="/article",method=RequestMethod.PUT)
public Object changeStatus(@RequestParam(defaultValue="0")Long id,@RequestParam(defaultValue="false") boolean v) {
	iArticle.changeStatus(id,v);
	Map<String,Object> m= new HashMap<>();
	return m;
}
@JsonView({View.Article.class})
@RequestMapping(value="/articles")
public Page<Article> getArticles(@RequestParam(defaultValue="") String type,@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="10")int size) {
	return iArticle.getArticlesByType(type,page, size);
}
// ce service est reserve pour le front end il doit pas etre securisé
@RequestMapping(value="/articles/criteria",method=RequestMethod.POST)
@JsonView({View.Article.class})
public Page<Article> getArticlesWithCriteria(@RequestBody() String [] types,
		@RequestParam(defaultValue="") String categorie,
		@RequestParam(defaultValue="") String titre,
		@RequestParam(defaultValue="") String pays,
		@RequestParam(defaultValue="0") int page,
		@RequestParam(defaultValue="10")int size,@RequestParam(defaultValue="0") Long id) {
	return iArticle.getArticles(titre, categorie, types, pays, true,id,page, size);
}

//ce service est reserve pour le front end il doit pas etre securisé
@RequestMapping(value="/articles/associe")
@JsonView({View.Article.class})
public List<Article> getArticlesAssociesWithCriteria(@RequestParam(defaultValue="") String type,
		@RequestParam(defaultValue="") Long id,
		@RequestParam(defaultValue="") String categorie,
		@RequestParam(defaultValue="") String titre,
		@RequestParam(defaultValue="") String pays,
		@RequestParam(defaultValue="0") int page,
		@RequestParam(defaultValue="10")int size) {
	return iArticle.ArticleAssocie(id, titre, categorie, type, pays, true);
}

@RequestMapping(value="/articles/get")
@JsonView({View.Article.class})
public Page<Article> getVisiblesArticles(@RequestParam(defaultValue="") String type,@RequestParam(defaultValue="0") int page, @RequestParam(defaultValue="10")int size) {
	return iArticle.getVisiblesArticlesByType(type,page, size);
}
@RequestMapping(value="/articles/structure")
@JsonView({View.Article.class})
public List<Article> getStructureArticles(@RequestParam(defaultValue="") String s) {
	return iArticle.getArticlesBySymbole(s);
}
@RequestMapping(value = "/image", method = RequestMethod.GET)
public void getImageAsByteArray(@RequestParam(defaultValue="") String photo,HttpServletResponse response) throws IOException {
    iArticle.displayImage(photo,response);
}
}
