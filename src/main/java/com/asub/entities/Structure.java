package com.asub.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotEmpty;

import com.asub.View;
import com.asub.metier.GeneratedStructureSeq;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class Structure implements Serializable{
	@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JsonView({View.Structure.class,View.Donnees.class,View.StructureWithBrmv.class})
	private GeneratedStructureSeq seq;
	@NotEmpty
	@JsonView({View.Structure.class,View.StructureWithBrmv.class,View.Donnees1.class})	
private String nom;
	@NotEmpty
	@Id
	@JsonView({View.Structure.class,View.Donnees.class,View.StructureWithBrmv.class})	
private String symbole;
	@NotEmpty
	@JsonView({View.Structure.class,View.StructureWithBrmv.class})
private String secteur;
@OneToMany(mappedBy="structure",cascade=CascadeType.ALL)
@JsonView({View.StructureWithBrmv.class})
private List<Donnees> donnees;
@Column(columnDefinition = "TEXT")
@JsonView({View.Structure.class,View.StructureWithBrmv.class})	
private String description;
@JsonView({View.StructureWithBrmv.class})
@Transient
private Donnees lastDonnee;
public Structure() {
	super();
	// TODO Auto-generated constructor stub
}

public Structure(String symbole) {
	super();
	this.symbole = symbole;
}

public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getSymbole() {
	return symbole;
}
public void setSymbole(String symbole) {
	this.symbole = symbole;
}
public String getSecteur() {
	return secteur;
}
public void setSecteur(String secteur) {
	this.secteur = secteur;
}
public List<Donnees> getDonnees() {
	return donnees;
}
public void setDonnees(List<Donnees> donnees) {
	this.donnees = donnees;
}

public GeneratedStructureSeq getSeq() {
	return seq;
}

public void setSeq(GeneratedStructureSeq seq) {
	this.seq = seq;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public Donnees getLastDonnee() {
	return lastDonnee;
}

public void setLastDonnee(Donnees lastDonnee) {
	this.lastDonnee = lastDonnee;
}

@Override
public boolean equals(Object s) {
	// TODO Auto-generated method stub
	if(s == null)                return false;
    if(!(s instanceof Structure)) return false;

    Structure other = (Structure) s;
    	return this.symbole.equalsIgnoreCase(other.symbole)&&this.nom.equalsIgnoreCase(other.nom);
}
@Override
public int hashCode() {
	// TODO Auto-generated method stub
	return this.symbole.hashCode()*this.nom.hashCode();
}
}
