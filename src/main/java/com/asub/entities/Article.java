package com.asub.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.asub.View;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class Article {
	@Id @GeneratedValue
	@JsonView({View.Article.class,View.User.class})
private Long id;
	@NotEmpty
	@JsonView({View.Article.class,View.User.class})
	private String titre;
@Column(columnDefinition = "TEXT")
@NotEmpty
@JsonView({View.Article.class,View.User.class})
private String contenu;
@DateTimeFormat(pattern="dd-mm-yyyy")
@JsonView({View.Article.class,View.User.class})
private Date creation;
@NotEmpty
@JsonView({View.Article.class,View.User.class})
private String type;
@JsonView({View.Article.class,View.User.class})
private boolean visibilite;
@JsonView({View.Article.class,View.User.class})
private String photoUrl;
@JsonView({View.Article.class,View.User.class})
private String categorie;
@JsonView({View.Article.class,View.User.class})
private String pays;
@JsonView({View.Article.class,View.User.class})
private String youtubeVideoId;
@ManyToOne
@JsonView({View.Article.class})
private User auteur;
@JsonView({View.Article.class,View.User.class})
private String symbole;
public Article() {
	super();
	// TODO Auto-generated constructor stub
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getContenu() {
	return contenu;
}
public void setContenu(String contenu) {
	this.contenu = contenu;
}
public Date getCreation() {
	return creation;
}
public void setCreation(Date creation) {
	this.creation = creation;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getTitre() {
	return titre;
}
public void setTitre(String titre) {
	this.titre = titre;
}
public boolean isVisibilite() {
	return visibilite;
}
public void setVisibilite(boolean visibilite) {
	this.visibilite = visibilite;
}
public String getPhotoUrl() {
	return photoUrl;
}
public void setPhotoUrl(String photoUrl) {
	this.photoUrl = photoUrl;
}
public String getCategorie() {
	return categorie;
}
public void setCategorie(String categorie) {
	this.categorie = categorie;
}
public String getPays() {
	return pays;
}
public void setPays(String pays) {
	this.pays = pays;
}
public String getYoutubeVideoId() {
	return youtubeVideoId;
}
public void setYoutubeVideoId(String youtubeVideoId) {
	this.youtubeVideoId = youtubeVideoId;
}
public User getAuteur() {
	return auteur;
}
public void setAuteur(User auteur) {
	this.auteur = auteur;
}
public String getSymbole() {
	return symbole;
}
public void setSymbole(String symbole) {
	this.symbole = symbole;
}

}
