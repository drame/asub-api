package com.asub.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Publicite {
	@Id @GeneratedValue
private Long id;
private String photoUrl;
private String url;
private boolean visibilite;
public Publicite() {
	super();
	// TODO Auto-generated constructor stub
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getPhotoUrl() {
	return photoUrl;
}
public void setPhotoUrl(String photoUrl) {
	this.photoUrl = photoUrl;
}
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}
public boolean isVisibilite() {
	return visibilite;
}
public void setVisibilite(boolean visibilite) {
	this.visibilite = visibilite;
}

}
