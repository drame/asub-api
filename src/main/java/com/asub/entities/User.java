package com.asub.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.asub.View;
import com.fasterxml.jackson.annotation.JsonView;
@Entity
public class User  implements Serializable{
@Id @GeneratedValue
@JsonView({View.Article.class,View.User.class,View.UserFront.class})
private Long id;
@JsonView({View.Article.class,View.User.class,View.UserFront.class})
@Email
private String email;
@NotEmpty
private String password;
@JsonView({View.Article.class,View.User.class,View.UserFront.class})
@Column(columnDefinition="TEXT")
@NotEmpty
private String description;
@JsonView({View.Article.class,View.User.class,View.UserFront.class})
@NotEmpty
private String nom;
@JsonView({View.Article.class,View.User.class,View.UserFront.class})
@NotEmpty
private String prenom;
private String role;
@OneToMany(mappedBy="auteur",cascade=CascadeType.REMOVE)
@JsonView({View.User.class})
private List<Article> articles;
public User() {
	super();
	// TODO Auto-generated constructor stub
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrenom() {
	return prenom;
}

public String getRole() {
	return role;
}
public void setRole(String role) {
	this.role = role;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}
public List<Article> getArticles() {
	return articles;
}
public void setArticles(List<Article> articles) {
	this.articles = articles;
}

}
