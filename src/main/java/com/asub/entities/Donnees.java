package com.asub.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.asub.View;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class Donnees implements Serializable {
@Id @GeneratedValue
@JsonView({View.Donnees.class,View.Donnees1.class})
private Long id;
@JsonView({View.Donnees.class,View.StructureWithBrmv.class,View.Donnees1.class})
private String var;
@JsonView({View.Donnees.class,View.StructureWithBrmv.class})
private String vol_titre;
@JsonView({View.Donnees.class,View.StructureWithBrmv.class})
private String vol_fcfa;
@JsonView({View.Donnees.class,View.StructureWithBrmv.class,View.Donnees1.class})
// le cours actuel est nommé cours_pre
private String cours_prec=new String();
@JsonView({View.Donnees.class,View.StructureWithBrmv.class})
private String veille;
@JsonView({View.Donnees.class,View.StructureWithBrmv.class,View.Donnees1.class})
@DateTimeFormat(pattern="dd-mm-yyyy")
private Date date;
@ManyToOne(cascade = {CascadeType.PERSIST})
@JsonView({View.Donnees.class,View.Donnees1.class})
private Structure structure;
public Donnees() {
	super();
	// TODO Auto-generated constructor stub
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}

public String getVar() {
	return var;
}
public void setVar(String var) {
	this.var = var;
}
public String getVol_titre() {
	return vol_titre;
}
public void setVol_titre(String volTitre) {
	this.vol_titre = volTitre;
}
public String getVol_fcfa() {
	return vol_fcfa;
}
public void setVol_fcfa(String volFcfa) {
	this.vol_fcfa = volFcfa;
}
public String getCours_prec() {
	return cours_prec;
}
public void setCours_prec(String cours_prec) {
	this.cours_prec = cours_prec;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
public Structure getStructure() {
	return structure;
}
public void setStructure(Structure structure) {
	this.structure = structure;
}

public String getVeille() {
	return veille;
}
public void setVeille(String veille) {
	this.veille = veille;
}
@Override
public boolean equals(Object d) {
	// TODO Auto-generated method stub
	if(d == null)                return false;
    if(!(d instanceof Donnees)) return false;

    Donnees other = (Donnees) d;
 
    	return this.date.equals(other.date)&&this.structure.equals(other.structure);
}

@Override
public int hashCode() {
	// TODO Auto-generated method stub
	return structure.hashCode()*date.hashCode();
}
}
