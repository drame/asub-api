package com.asub.entities;

import org.springframework.security.core.GrantedAuthority;

public class Role implements GrantedAuthority {
private String role;

	public Role(String role) {
	super();
	this.role = role;
}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return role;
	}

}
