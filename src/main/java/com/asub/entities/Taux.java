package com.asub.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Taux {
	@Id
private String libelle;
private String valeur;
public Taux(String libelle, String valeur) {
	super();
	this.libelle = libelle;
	this.valeur = valeur;
}
public Taux() {
	super();
	// TODO Auto-generated constructor stub
}
public String getLibelle() {
	return libelle;
}
public void setLibelle(String libelle) {
	this.libelle = libelle;
}
public String getValeur() {
	return valeur;
}
public void setValeur(String valeur) {
	this.valeur = valeur;
}


}
