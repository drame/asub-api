package com.asub.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Evenement {
	@Id @GeneratedValue
private Long id;
	@NotEmpty
private String titre;
	@NotEmpty
@Column(columnDefinition = "TEXT")
private String description;
	@NotEmpty
private String categorie;
	@NotEmpty
private String type;
private String photoUrl;
@DateTimeFormat(pattern="dd-mm-yyyy")
private Date date;
@DateTimeFormat(pattern="dd-mm-yyyy")
private Date fin;
private boolean visibilite;
private String lieu;
public Evenement() {
	super();
	// TODO Auto-generated constructor stub
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getTitre() {
	return titre;
}
public void setTitre(String titre) {
	this.titre = titre;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getCategorie() {
	return categorie;
}
public void setCategorie(String categorie) {
	this.categorie = categorie;
}
public String getPhotoUrl() {
	return photoUrl;
}
public void setPhotoUrl(String photoUrl) {
	this.photoUrl = photoUrl;
}
public boolean isVisibilite() {
	return visibilite;
}
public void setVisibilite(boolean visibilite) {
	this.visibilite = visibilite;
}
public Date getDate() {
	return date;
}
public void setDate(Date date) {
	this.date = date;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public Date getFin() {
	return fin;
}
public void setFin(Date fin) {
	this.fin = fin;
}
public String getLieu() {
	return lieu;
}
public void setLieu(String lieu) {
	this.lieu = lieu;
}

}
