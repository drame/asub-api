package com.asub.security;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.asub.security.SecurityConfigurationUtilis.RestAccessDeniedHandler;
import com.asub.security.SecurityConfigurationUtilis.RestAuthenticationFailureHandler;
import com.asub.security.SecurityConfigurationUtilis.RestAuthenticationSuccessHandler;
import com.asub.security.SecurityConfigurationUtilis.RestUnauthorizedEntryPoint;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity()
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	@Autowired
private RestAccessDeniedHandler restAccessDeniedHandler;
	@Autowired
	private RestAuthenticationFailureHandler restAuthenticationFailureHandler;
	@Autowired
	private RestUnauthorizedEntryPoint restUnauthorizedEntryPoint;
	@Autowired
	private RestAuthenticationSuccessHandler restAuthenticationSuccessHandler;
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private AuthenticationProvider2  authenticationProvider2;
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// TODO Auto-generated method stub
		auth.authenticationProvider(authenticationProvider2);
		//auth.userDetailsService(userDetailsService);
	}
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		http.csrf().disable()
		.cors()
		.and()
		.headers().disable()
		.authorizeRequests()
		.antMatchers("/article/get",
				"/articles/criteria",
				"/articles/associe",
				"/articles/structure",
				 "/articles/get",
				 "/image",
				 "/donnees/get",
				 "/palmares",
				 "/evenement/get",
				 "/evenement/get",
				 "/evenements/criteria",
				 "/evenements/get",
				 "/evenements/associe",
				 "/publicites/get",
				 "/allStructures",
				 "/structure",
				 "/structures/get",
				 "/taux",
				 "/users/get").permitAll()
		.anyRequest().authenticated()
		.and()
		.formLogin()
		.loginProcessingUrl("/login").permitAll()
		.successHandler(restAuthenticationSuccessHandler)
		.failureHandler(restAuthenticationFailureHandler)		
		.usernameParameter("email")
		.passwordParameter("password")
		.permitAll()
		.and()
		.exceptionHandling()
		.authenticationEntryPoint(restUnauthorizedEntryPoint)
		.accessDeniedHandler(restAccessDeniedHandler)
		.and()
		.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login")
		//.and().rememberMe().rememberMeParameter("rememberme")
		//.tokenRepository(persistentTokenRepository())
		//.tokenValiditySeconds(1209600)
		;
	}
	
	@Override
    public void configure(WebSecurity web) throws Exception {
        web
           .ignoring()
           .antMatchers("/sign-in.html","/index.html","/css/**","/js/**","/images/**","/fonts/**","/views/**"); // #3
    }

	}


