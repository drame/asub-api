package com.asub.security;


import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import com.asub.dao.UserRepository;
import com.asub.entities.Role;
import com.asub.entities.User;
@Service
public class AuthenticationProvider2 implements AuthenticationProvider {
	@Autowired
	private UserRepository usersRepository;
		@Override
		public Authentication authenticate(Authentication auth) throws AuthenticationException {
			// TODO Auto-generated method stub
			String email=(String) auth.getPrincipal();
			
			String password =(String) auth.getCredentials();
			System.out.println("email "+email+" pass "+password);
			User user=usersRepository.findByEmail(email);
			System.out.println("authentifactionnnnnnnnnnnnn");
			if(user==null){
				System.out.println("nom d'utilisateur introuvable");
				throw new BadCredentialsException("nom d'utilisateur introuvable");
			}
			if(!user.getPassword().equals(password)){
				System.out.println("Mot de passe incorrect");
				throw new BadCredentialsException("Mot de passe incorrect");
			}
			String r = user.getRole();
			Role role = new Role(r);
			Collection<Role> roles= new ArrayList<Role>();
			roles.add(role);
			boolean droit =true;
			/*for(Roles r:roles)
			{ if(r.getRole().equals("ADMIN") || r.getRole().equals("GESTIONNAIRE"))
			   {
				droit=false;
				break;
			   }
			}
			if(!droit) throw new BadCredentialsException("Vous avez le droit d'accés ici");*/
			UsernamePasswordAuthenticationToken token = new  UsernamePasswordAuthenticationToken(email, password,roles);
			//token.setDetails(user);
			return token;
		}

		@Override
		public boolean supports(Class<?> authentication) {
			// TODO Auto-generated method stub
			return authentication.equals(UsernamePasswordAuthenticationToken.class);
		}

}
