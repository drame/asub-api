package com.asub.metier;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asub.dao.TauxRepository;
import com.asub.entities.Taux;
@Service
public class TauxImpl implements ITaux{
@Autowired
private TauxRepository tRep;
	@Override
	public List<Taux> getTaux() {
		// TODO Auto-generated method stub
		
		return tRep.findAll();
	}

	@Override
	public Object update(List<Taux> taux) {
		// TODO Auto-generated method stub
		Map<String, Object> m= new HashMap<>();
		try{
			tRep.save(taux);
			m.put("erreur", false);
			m.put("message", "Données modifiées avec succès!!");
		}
		catch (Exception e) {
			// TODO: handle exception
			m.put("erreur", true);
			m.put("message", "Veuillez bien vérifier les données");
		}
		finally {
			return m;
		}
	
		
	}

}
