package com.asub.metier;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import com.asub.entities.Article;

public interface IArticle {
public Article addArticle(Article a,MultipartFile photo) throws Exception;
public void deleteArticle(Long id);
public void changeStatus(Long id,boolean s);
public Page<Article> getArticles(String titre,String categorie,String [] types,String pays, boolean visibilite,Long id,int page,int size);
public Page<Article> getArticlesByType(String type,int page,int size);
public Page<Article> getVisiblesArticlesByType(String type,int page,int size);
public Page<Article> getVisiblesArticles(int page,int size);
public Article getArticle(Long id);
public void displayImage(String photo,HttpServletResponse response);
public List<Article> ArticleAssocie(Long id,String titre, String categorie, String type, String pays, boolean visibilite);
public List<Article> getArticlesBySymbole(String s);
}
