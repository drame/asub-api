package com.asub.metier;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.asub.entities.Article;
import com.asub.entities.Evenement;

public interface IEvenement {
public Page<Evenement> getEvenements(Pageable p);
public Page<Evenement> getEvenements(String type, String categorie,String titre,String lieu,Date fin,boolean v,Pageable p);
public void changeStatus(Long id,boolean s);
public Page<Evenement> getEvenementByType(String type,Pageable p);
public Page<Evenement> getVisiblesEvenementsByType(String type, Pageable p);
public void addEvenement(Evenement e,MultipartFile photo)throws Exception;
public void deleteEvenement(Long id);
public Evenement getEvenement(Long id);
public Page<Evenement> evenementAssocie(String type,String c,Pageable p);

}
