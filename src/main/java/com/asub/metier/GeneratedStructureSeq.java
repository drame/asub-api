package com.asub.metier;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.asub.View;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
public class GeneratedStructureSeq implements Serializable{
	 @Id
	  @GeneratedValue()
	 @JsonView({View.Structure.class,View.Donnees.class})
	 private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	 
}
