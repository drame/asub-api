package com.asub.metier;

import java.io.File;

import org.springframework.web.multipart.MultipartFile;

public class Utilis {
	public static String  upload(String photoUrl,MultipartFile photo) throws Exception{
		// TODO Auto-generated method stub
		if(photo!=null){
			if(!photo.getOriginalFilename().endsWith("jpeg")&&
					!photo.getOriginalFilename().endsWith("jpg")&&
					!photo.getOriginalFilename().endsWith("png")&&
			!photo.getOriginalFilename().endsWith("gif"))
				throw new Exception("Format de la photo insupportable");
		try {
			System.out.println(photo.getOriginalFilename());
            String uploadsDir = "/uploads/";
            String realPathtoUploads =  System.getProperty("user.dir")+uploadsDir;
            System.out.println(realPathtoUploads);
            if(! new File(realPathtoUploads).exists())
            {
                new File(realPathtoUploads).mkdir();
            }
            int  point=photo.getOriginalFilename().lastIndexOf(".");
            String orgName = photo.getOriginalFilename().substring(0, point)+"_"+System.currentTimeMillis()+photo.getOriginalFilename().substring(point);
            String filePath = realPathtoUploads + orgName;
            File dest = new File(filePath);
            photo.transferTo(dest);
            if(photoUrl!=null){
            	deletePhoto(realPathtoUploads+photoUrl);
            }
            return orgName;
		}
		catch (Exception e) {
			throw new Exception("Une erreur est survenue lors de l'upload");
		}
		}
		return null;
	}
	public static void deletePhoto(String path) throws Exception{
		try{

			File file = new File(path);

			if(file.delete()){
				System.out.println(file.getName() + " is deleted!");
			}else{
				System.out.println("Delete operation is failed.");
			}

		}catch(Exception e){

			throw new Exception("Erreur de supprimer de la photo précedente");

		}
	}
}
