package com.asub.metier;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.asub.AsubException;
import com.asub.entities.User;

public interface IUser {
public Page<User> getPageOfUser(Pageable p);
public List<User> getAllUser();
public void addUser(User u) throws AsubException;
public void deleteUser(Long id);
public User userByEmail(String email);
public User getUser(String name);
}
