package com.asub.metier;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.asub.dao.EvenementRepository;
import com.asub.dao.SpecificationDao;
import com.asub.entities.Article;
import com.asub.entities.Evenement;
@Service
public class EvenementImpl implements IEvenement {
@Autowired
private EvenementRepository eRep;
	@Override
	public Page<Evenement> getEvenements(Pageable p) {
		// TODO Auto-generated method stub
		return eRep.findAllByOrderByDateAsc(p);
	}


	@Override
	public void addEvenement(Evenement e,MultipartFile photo)throws Exception {
		// TODO Auto-generated method stub
		if(photo==null&&e.getId()==null) throw new Exception("La photo est obligatoire");
		if(e.getDescription().isEmpty()) throw new Exception("La description est obligatoire");
			if(e.getDescription().length()>300){
				throw new Exception("La contenu d'un évenement ne doit exceder les 300 caractéres");
			}
		
		String ph=Utilis.upload(e.getPhotoUrl(), photo);
		if(ph!=null)
		e.setPhotoUrl(ph);
		eRep.save(e);
	}

	@Override
	public void deleteEvenement(Long id) {
		// TODO Auto-generated method stub
		Evenement e= eRep.findOne(id);
		eRep.delete(id);
		if(e.getPhotoUrl()!=null){
			try {
				Utilis.deletePhoto(e.getPhotoUrl());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
	}


	@Override
	public void changeStatus(Long id, boolean s) {
		// TODO Auto-generated method stub
		Evenement e = eRep.findOne(id);
		e.setVisibilite(s);
		eRep.save(e);
	}


	@Override
	public Page<Evenement> getEvenementByType(String type, Pageable p) {
		// TODO Auto-generated method stub
		if(type.isEmpty()){
			return eRep.findAllByOrderByDateAsc(p);
		}
		return eRep.findByType(type, p);
	}


	@Override
	public Page<Evenement> getVisiblesEvenementsByType(String type, Pageable p) {
		// TODO Auto-generated method stub
		return eRep.findVisiblesEvenementsByType(type,p);
	}


	@Override
	public Evenement getEvenement(Long id) {
		// TODO Auto-generated method stub
		return eRep.findOne(id);
	}


	@Override
	public Page<Evenement> evenementAssocie(String type,String c, Pageable p) {
		// TODO Auto-generated method stub
		if(c!=null){
			if(!c.isEmpty()){
				return eRep.findVisibiliteAndCategoryAndTypeByOrderByAsc(c,type,p);
			}
		}
		return eRep.findVisiblesEvenementsByType(type, p);
	}


	@Override
	public Page<Evenement> getEvenements(String type, String categorie,String titre, String lieu, Date fin, boolean v, Pageable p) {
		// TODO Auto-generated method stub
		return eRep.findAll(Specifications.where(SpecificationDao.EvenementAvecCategorie(categorie))
				.and(SpecificationDao.EvenementAvecDateFin(fin))
				.and(SpecificationDao.EvenementAvecTitre(titre))
				.and(SpecificationDao.EvenementAvecType(type))
				.and(SpecificationDao.EvenementAvecVisibilite(v))
				,p);
	}

}
