package com.asub.metier;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;

import com.asub.AsubException;
import com.asub.entities.Donnees;

public interface IDonnees {
public void addDonnees(Donnees d) throws AsubException;
public void deleteDonnees(Long id);
public Donnees getDonnees(Long id);
public Page<Donnees> getAllDonnees(String s,int page,int size);
public List<Donnees> getAllDonnees2(String s);
public List<Donnees> findAll();
public Set<Donnees> saveAll(Set<Donnees> setDonnees);
public Donnees addDonnees1(Donnees d);
public void deleteStructureDonnees(String s);
public Object palmares();
}
