package com.asub.metier;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import com.asub.entities.Publicite;

public interface IPublicite {
public void addPublicite(Publicite p,MultipartFile photo) throws Exception;
public void deletePublicite(Long id);
public void changeStatus(Long id,boolean s);
public Page<Publicite> getPublicites(Boolean v, int size,int page);
public Publicite getPublicite(Long id);
}
