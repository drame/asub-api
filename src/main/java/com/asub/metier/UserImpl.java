package com.asub.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.asub.AsubException;
import com.asub.dao.UserRepository;
import com.asub.entities.User;

@Service
public class UserImpl implements IUser {
	
@Autowired 
private UserRepository uRep;
	@Override
	public Page<User> getPageOfUser(Pageable p) {
		// TODO Auto-generated method stub
		return uRep.findAll(p);
	}

	@Override
	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		return uRep.findAll();
	}

	@Override
	public void addUser(User u) throws AsubException {
		// TODO Auto-generated method stub
		if(u!=null){
			User u1=uRep.findByEmail(u.getEmail());
			if(u.getId() != null){
				if(u1!=null){
					if(u1.getId()!=u.getId()){
						System.out.println(u.getNom()+"ici");
						throw new AsubException("L'adresse email existe déjà");
					}
				}
			}
			else{
				if(u1!=null) {
					throw new AsubException("L'adresse email existe déjà");
				}
			}
			uRep.save(u);
		}
		
	}

	@Override
	public void deleteUser(Long id) {
		// TODO Auto-generated method stub
		uRep.delete(id);
	}

	@Override
	public User userByEmail(String email) {
		// TODO Auto-generated method stub
		return uRep.findByEmail(email);
	}

	@Override
	public User getUser(String name) {
		// TODO Auto-generated method stub
		return uRep.findByEmail(name);
	}

}
