package com.asub.metier;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import com.asub.AsubException;
import com.asub.entities.Structure;

public interface IStructure {
public Page<Structure> getStructures(int page, int size);
public List<Structure> getStructure(int brvm);
public List<Structure> getAllStructures();
public Structure getStructure(String s);
public Structure editStructure(Structure s);
public Structure addStructure(Structure s) throws AsubException;
public Structure addStructure1(Structure s);
public void deleteStructure(String s);
public void deleteAll();
public void importExcel(MultipartFile file) throws Exception;

}
