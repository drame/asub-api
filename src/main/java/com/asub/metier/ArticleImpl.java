package com.asub.metier;

import static org.hamcrest.CoreMatchers.isA;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.asub.dao.ArticleRepository;
import com.asub.dao.SpecificationDao;
import com.asub.entities.Article;
@Service
public class ArticleImpl implements IArticle {
@Autowired 
private ArticleRepository articleRepository;
@Autowired
private HttpServletRequest request;
@Autowired
ServletContext servletContext;
	@Override
	@Transactional
	public Article addArticle(Article a,MultipartFile photo) throws Exception{
		// TODO Auto-generated method stub
		if(photo==null&&a.getId()==null) throw new Exception("La photo est obligatoire");
		String ph=Utilis.upload(a.getPhotoUrl(), photo);
		if(ph!=null)
		a.setPhotoUrl(ph);
		return articleRepository.save(a);
	}

	@Override
	@Transactional
	public void deleteArticle(Long id) {
		// TODO Auto-generated method stub
		Article a= articleRepository.findOne(id);
		articleRepository.delete(id);
		if(a.getPhotoUrl()!=null){
			try {
				Utilis.deletePhoto(a.getPhotoUrl());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void changeStatus(Long id,boolean s) {
		// TODO Auto-generated method stub
		Article a= articleRepository.findOne(id);
		if(a!=null){
			a.setVisibilite(s);
			articleRepository.save(a);
		}
		
		
	}



	@Override
	public Page<Article> getArticlesByType(String type,int page, int size) {
		// TODO Auto-generated method stub
		if(type!=null){
			if(type.length()>0){
				return articleRepository.getArticles(type, new PageRequest(page, size));
			}
		}
		return articleRepository.findAllByOrderByCreationAsc(new PageRequest(page, size));
	}

	@Override
	public Article getArticle(Long id) {
		// TODO Auto-generated method stub
		return articleRepository.findOne(id);
	}

	@Override
	public Page<Article> getVisiblesArticlesByType(String type, int page, int size) {
		// TODO Auto-generated method stub
		if(type!=null){
			if(type.length()>2){
				return articleRepository.getVisiblesArticles(type, new PageRequest(page, size));
			}
		}
		return articleRepository.findVisibiliteByOrderByCreationAsc(new PageRequest(page, size));
	}
// pas encore utiliser
	@Override
	public Page<Article> getVisiblesArticles(int page, int size) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void displayImage(String photoUrl,HttpServletResponse response) {
		// TODO Auto-generated method stub
		 String uploadsDir = "/uploads/";
         String realPathtoUploads =  System.getProperty("user.dir")+uploadsDir;
         System.out.println("uploads chemin "+realPathtoUploads);
		
	    try {
	    	FileInputStream input = new FileInputStream(new File(realPathtoUploads+photoUrl));
	    	String url=photoUrl.toLowerCase();
	    	if(url.endsWith("jpeg")||url.endsWith("jpg")){
	    		System.out.println("jpg");
	    	response.setContentType(MediaType.IMAGE_JPEG_VALUE);
	    	}
	    	else if(url.endsWith("gif")){
	    		System.out.println("gif");
		    	response.setContentType(MediaType.IMAGE_GIF_VALUE);
	    	}
	    	else{
	    		System.out.println("png");
	    	    	response.setContentType(MediaType.IMAGE_PNG_VALUE);
	    	}
			 IOUtils.copy(input,response.getOutputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("erreur lecture fichier "+e);
		}
	}
	@Override
	public List<Article> ArticleAssocie(Long id, String titre, String categorie, String type, String pays, boolean visibilite) {
		// TODO Auto-generated method stub
		if(!titre.isEmpty()){
			titre="%titre"+titre+"%";
		}
		String t[]={type};
		return articleRepository.findAll(Specifications.where(SpecificationDao.ArticleAvecTitre(titre))
				.and(SpecificationDao.ArticleAvecCategorie(categorie))
				.and(SpecificationDao.ArticleAvecType(t))
				.and(SpecificationDao.ArticleAvecPays(pays))
				.and(SpecificationDao.ArticleAvecVisibilite(visibilite))
				.and(SpecificationDao.ArticleDifferent(id))
				,  new PageRequest(0, 4)).getContent();
	}

	@Override
	public Page<Article> getArticles(String titre, String categorie, String [] types, String pays, boolean visibilite,Long idAuteur,
			int page, int size) {
		// TODO Auto-generated method stub
		return articleRepository.findAll(Specifications.where(SpecificationDao.ArticleAvecTitre(titre))
				.and(SpecificationDao.ArticleAvecCategorie(categorie))
				.and(SpecificationDao.ArticleAvecType(types))
				.and(SpecificationDao.ArticleAvecPays(pays))
				.and(SpecificationDao.ArticleAvecAuteur(idAuteur))
				.and(SpecificationDao.ArticleAvecVisibilite(visibilite))
				,  new PageRequest(page, size));
	}

	@Override
	public List<Article> getArticlesBySymbole(String s) {
		// TODO Auto-generated method stub
		return articleRepository.findBySymboleAndVisibilite(s, true);
	}

}
