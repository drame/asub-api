package com.asub.metier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.asub.AsubException;
import com.asub.dao.StructureRepository;
import com.asub.entities.Donnees;
import com.asub.entities.Structure;


@Service
public class StructureImpl implements IStructure {
@Autowired
private StructureRepository structureRepository;
@Autowired
private IDonnees iDonnees;
	@Override
	public Page<Structure> getStructures(int page,int size) {
		// TODO Auto-generated method stub
		return structureRepository.findAll(new PageRequest(page, size));
	}

	@Override
	public Structure getStructure(String s) {
		// TODO Auto-generated method stub
		return structureRepository.findOne(s);
	}

	@Override
	public Structure editStructure(Structure s) {
		// TODO Auto-generated method stub
		
		return  structureRepository.save(s);
	}

	@Override
	@Transactional
	public void deleteStructure(String s) {
		// TODO Auto-generated method stub
		iDonnees.deleteStructureDonnees(s);
		structureRepository.delete(s);
	}

	@Override
	public Structure addStructure(Structure s) throws AsubException{
		// TODO Auto-generated method stub
		List<Structure> s1;
		s1=structureRepository.findBySymbole(s.getSymbole());
		if(s1!=null&&s.getSeq()==null){
			 throw new AsubException("Le symbole existe déja");
		}
		else{
			for(Structure ss:s1){
				if(ss.getSeq().getId()!=s.getSeq().getId()) throw new AsubException("Le symbole existe déja");
			}
		}
		if(s.getSeq()==null)
		{
			GeneratedStructureSeq seq= new GeneratedStructureSeq();
			s.setSeq(seq);
		}
		return  structureRepository.save(s);
	}

	@Override
	
	public List<Structure> getStructure(int brvm) {
		// TODO Auto-generated method stub
		List<Structure> structures1= new ArrayList<>();
		int index1 = 0,index2=0;
		List<Structure> structures=structureRepository.findAll();
			for(int i=0; i<structures.size();i++){
				Structure s=structures.get(i);
		
				if(s.getDonnees()!=null){
					Collections.sort(s.getDonnees(), new Comparator<Donnees>() {

						@Override
						public int compare(Donnees o1, Donnees o2) {
							// TODO Auto-generated method stub
							return o1.getDate().compareTo(o2.getDate());
						}
					});
				}
				s.setLastDonnee(s.getDonnees().get(s.getDonnees().size()-1));
				if(s.getSymbole().toLowerCase().indexOf("brvm")<0 && brvm==1){
					s.setDonnees(null);
				}
				else if(brvm==0){
					s.setDonnees(null);
				}
				if(s.getNom().toLowerCase().indexOf("brvm 10")>=0){
					index1=i;
				}
				if(s.getNom().toLowerCase().indexOf("brvm composite")>=0){
					index2=i;
				}
				if(s.getNom().toLowerCase().indexOf("brvm")==-1){
					structures1.add(s);
				}
			}
			System.out.println("size"+structures1.size());
			Collections.sort(structures1, new Comparator<Structure>() {

				@Override
				public int compare(Structure o1, Structure o2) {
					// TODO Auto-generated method stub
					return o1.getNom().compareTo(o2.getNom());
				}
			});
			structures1.add(0, structures.get(index1));
			structures1.add(1, structures.get(index2));
			
		return structures1;
	}

	@Override
	public void importExcel(MultipartFile file) throws Exception {
		// TODO Auto-generated method stub
		try{
		List<Donnees> donnees=importToStructures(file);
		List<Donnees> donneesDB= iDonnees.findAll();
		Set<Donnees> setDonnees= new HashSet<>();
		setDonnees.addAll(donneesDB);
		setDonnees.addAll(donnees);
		System.out.println("length "+setDonnees.size());
		iDonnees.saveAll(setDonnees);
			}
		catch (IOException e) {
			// TODO: handle exception
			System.out.println(e);
		}
		
	}
	private List<Donnees> importToStructures(MultipartFile file) throws IOException{
		List<Donnees> donnees= new ArrayList<>();
		int i = 0;
		// Creates a workbook object from the uploaded excelfile
		XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
		// Creates a worksheet object representing the first sheet
		XSSFSheet worksheet = workbook.getSheetAt(0);
		System.out.println("woorkbook"+workbook);
		System.out.println("worksheet"+worksheet);
		// Reads the data in excel file until last row is encountered
		System.out.println("first"+worksheet.getFirstRowNum());
		System.out.println("sheetName"+worksheet.getSheetName());
		System.out.println("last"+worksheet.getLastRowNum());
		while (i <= worksheet.getLastRowNum()) {
			// Creates an object for the UserInfo Model
			Structure s = new Structure();
			
			// Creates an object representing a single row in excel
			XSSFRow row = worksheet.getRow(i);
			if(row!=null){
				String col=row.getCell(0).getStringCellValue();
				if(!col.equalsIgnoreCase("secteur")&&!col.equalsIgnoreCase("titre")
						&&!col.equalsIgnoreCase("symbole")&&!col.equalsIgnoreCase("date")&&!col.equalsIgnoreCase("titre volume"))
				{   
					Donnees d= new Donnees();
					System.out.println(row.getCell(6).getRawValue());
					try{
						Double dd=Double.parseDouble(row.getCell(6).getRawValue());
					d.setVar(dd.toString());
					d.setVol_titre(row.getCell(7)!=null?row.getCell(7).getRawValue():null);
					d.setCours_prec(row.getCell(5).getRawValue());
					d.setVeille(row.getCell(4).getRawValue());
					d.setDate(row.getCell(1).getDateCellValue());
					s.setNom(row.getCell(2).getStringCellValue());
					s.setSecteur(row.getCell(0).getStringCellValue());
					s.setSymbole(row.getCell(3).getStringCellValue());
					d.setStructure(s);
					if(!donnees.contains(d))
					donnees.add(d);
					}
					catch (NumberFormatException e) {
						// TODO: handle exception
					}
			}
			}
			i++;
		}
		return donnees;
	}

	@Override
	public Structure addStructure1(Structure s) {
		// TODO Auto-generated method stub
		s.setDonnees(null);
		GeneratedStructureSeq seq= new GeneratedStructureSeq();
		s.setSeq(seq);
		return structureRepository.save(s);
	}

	@Override
	public List<Structure> getAllStructures() {
		// TODO Auto-generated method stub
		return structureRepository.findAll();
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		structureRepository.deleteAll();
		
	}


}
