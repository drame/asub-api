package com.asub.metier;

import java.util.List;

import com.asub.entities.Taux;

public interface ITaux {
public List<Taux> getTaux();
public Object update(List<Taux> taux);
}
