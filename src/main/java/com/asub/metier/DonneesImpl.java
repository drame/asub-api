package com.asub.metier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.asub.AsubException;
import com.asub.dao.DonneesRepository;
import com.asub.entities.Donnees;
import com.asub.entities.Structure;
@Service
public class DonneesImpl implements IDonnees {
@Autowired 
private DonneesRepository donneesRepository;
@Autowired
private IStructure iStructure;
	@Override
	public void addDonnees(Donnees d) throws AsubException {
		// TODO Auto-generated method stub
		Structure s= new Structure(d.getStructure().getSymbole());
		List<Donnees> donnees=donneesRepository.findByStructure(s);
		if(donnees.size()>0){
			for(Donnees d1:donnees){
				if(d1.getDate().equals(d.getDate())){
					throw new AsubException("La structure posséde une donnée concernant la date indiqué");
				}
			}
		}
		d.setStructure(s);
		donneesRepository.save(d);
		
	}

	@Override
	public void deleteDonnees(Long id) {
		// TODO Auto-generated method stub
		donneesRepository.delete(id);
	}

	@Override
	public Donnees getDonnees(Long id) {
		// TODO Auto-generated method stub
		return donneesRepository.findOne(id);
	}

	@Override
	public Page<Donnees> getAllDonnees(String s,int page,int size) {
		// TODO Auto-generated method stub
		return donneesRepository.findByStructureOrderByDate(s, new PageRequest(page, size));
	}

	@Override
	public List<Donnees> findAll() {
		// TODO Auto-generated method stub
		return donneesRepository.findAll();
	}

	@Override
	public Set<Donnees> saveAll(Set<Donnees> donnees) {
		// TODO Auto-generated method stub
		for(Donnees d: donnees){
			d=addDonnees1(d);
		}
		return donnees;
	}

	@Override
	public Donnees addDonnees1(Donnees d) {
		// TODO Auto-generated method stub
		if(d.getStructure()!=null){
			Structure s= iStructure.getStructure(d.getStructure().getSymbole());
			if(s==null){
				s=iStructure.addStructure1(d.getStructure());
			}
			d.setStructure(s);
			if(d.getId()==null||d.getId()==0)
			return donneesRepository.save(d);
			return null;
		}
		else{
			return null;
		}
		
	}

	@Override
	public void deleteStructureDonnees(String s) {
		// TODO Auto-generated method stub
		donneesRepository.deleteStructureDonnees(s);
		
	}

	@Override
	public List<Donnees> getAllDonnees2(String s) {
		// TODO Auto-generated method stub
		return donneesRepository.findAllByStructureOrderByDate(s);
	}

	@Override
	public Object palmares() {
		// TODO Auto-generated method stub
		List<Structure> structures=iStructure.getAllStructures();
		List<Donnees> lastDonneesOfStructures= new ArrayList<>();
		List<Donnees> baisses= new ArrayList<>(),hausses= new ArrayList<>();
		for(Structure s:structures){
			s.setDonnees(donneesRepository.findAllByStructureOrderByDate(s.getSymbole()));
			lastDonneesOfStructures.add(s.getDonnees().get(s.getDonnees().size()-1));
		}
		Collections.sort(lastDonneesOfStructures, new Comparator<Donnees>() {

			@Override
			public int compare(Donnees o1, Donnees o2) {
				// TODO Auto-generated method stub
				if(Double.parseDouble(o1.getVar())-Double.parseDouble(o2.getVar())<0){
					return -1;
				}
				else if(Double.parseDouble(o1.getVar())-Double.parseDouble(o2.getVar())>0){
					return 1;
					
				}
				return 0;
			}
		});
		baisses=lastDonneesOfStructures.subList(0, 5);

		hausses=lastDonneesOfStructures.subList(lastDonneesOfStructures.size()-5, lastDonneesOfStructures.size());
		Map<String,Object> m= new HashMap<>();
		m.put("baisses", baisses);
		Collections.reverse(hausses);
		m.put("hausses", hausses);
		return m;
	}

}
