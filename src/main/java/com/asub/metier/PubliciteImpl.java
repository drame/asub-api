package com.asub.metier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.asub.dao.PubliciteRepository;
import com.asub.dao.SpecificationDao;
import com.asub.entities.Publicite;
@Service
public class PubliciteImpl implements IPublicite{
@Autowired
private PubliciteRepository rep;
	@Override
	public void addPublicite(Publicite p, MultipartFile photo) throws Exception {
		// TODO Auto-generated method stub
		if(photo==null&&p.getId()==null) throw new Exception("La photo est obligatoire");
		String ph=Utilis.upload(p.getPhotoUrl(), photo);
		if(ph!=null)
		p.setPhotoUrl(ph);
		rep.save(p);
	}

	@Override
	@Transactional
	public void deletePublicite(Long id) {
		// TODO Auto-generated method stub
		Publicite p=rep.findOne(id); 
		rep.delete(id);
		if(p.getPhotoUrl()!=null){
			try {
				Utilis.deletePhoto(p.getPhotoUrl());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void changeStatus(Long id, boolean s) {
		// TODO Auto-generated method stub
		Publicite p=rep.findOne(id); 
		if(p!=null){
			p.setVisibilite(s);
			rep.save(p);
		}
	}

	@Override
	public Page<Publicite> getPublicites(Boolean v, int size, int page) {
		// TODO Auto-generated method stub
		return rep.findAll(Specifications.where(SpecificationDao.PubliciteAvecVisibilite(v)) ,new PageRequest(page, size));
	}

	@Override
	public Publicite getPublicite(Long id) {
		// TODO Auto-generated method stub
		return  rep.findOne(id);
	}

}
