package com.asub.pojo;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class Users implements UserDetails{
private boolean login; 
private String email;
private String nom;
private String role;
public Users() {
	super();
	// TODO Auto-generated constructor stub
}
public Users(String email, String nom, String role,boolean l) {
	super();
	this.email = email;
	this.nom = nom;
	this.role = role;
	this.login=l;
}

public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getRole() {
	return role;
}
public void setRole(String role) {
	this.role = role;
}

public boolean isLogin() {
	return login;
}
public void setLogin(boolean login) {
	this.login = login;
}
@Override
public Collection<? extends GrantedAuthority> getAuthorities() {
	// TODO Auto-generated method stub
	return null;
}
@Override
public String getPassword() {
	// TODO Auto-generated method stub
	return null;
}
@Override
public boolean isAccountNonExpired() {
	// TODO Auto-generated method stub
	return false;
}
@Override
public boolean isAccountNonLocked() {
	// TODO Auto-generated method stub
	return false;
}
@Override
public boolean isCredentialsNonExpired() {
	// TODO Auto-generated method stub
	return false;
}
@Override
public boolean isEnabled() {
	// TODO Auto-generated method stub
	return false;
}
@Override
public String getUsername() {
	// TODO Auto-generated method stub
	return null;
}

}
